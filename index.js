'use strict';

var gutil = require('gulp-util');
var through = require('through2');
var chalk = require('chalk');
var objectAssign = require('object-assign');

function gulp_log(message, opts) {
  opts = objectAssign(gulp_log.defaults, opts);

  if (opts.title === '') {
    opts.title = gulp_log.defaults.title;
  }

  return through.obj(function (file, enc, cb) {
    cb(null, file);
  }, function(cb) {
    gutil.log(gulp_log.levels[opts.level](chalk.bold(opts.title) + ': ' + message));
    cb();
  });
}

gulp_log.levels = {
  'fatal': chalk.bgRed,
  'error': chalk.red,
  'warn': chalk.yellow,
  'info': chalk.green,
  'debug': chalk.blue
}

gulp_log.defaults = {
  'title': 'gulp-log',
  'level': 'info'
}

module.exports = gulp_log;
