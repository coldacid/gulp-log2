/* global afterEach,beforeEach,describe,it */
'use strict';

var sinon = require('sinon');
var proxyquire = require('proxyquire');
var stripAnsi = require('strip-ansi');

var gutil = require('gulp-util');

var gutilStub = {
  log: function () {
    gutil.log.apply(gutil.log, arguments);
  }
}

var log2 = proxyquire('./', {
  'gulp-util': gutilStub
});

beforeEach(function () {
  sinon.spy(gutilStub, 'log');
})

afterEach(function () {
  gutilStub.log.restore();
})
