# gulp-log2
> Pipe-inlined logging for `gulp`

[![NPM version][npm-image]][npm-url] [![Build Status][ci-image]][ci-url] [![Dependency Status][depstat-image]][depstat-url]

## Install
```
$ npm install --save-dev gulp-log2
```

## Usage
```javascript
var gulp = require('gulp');
var log = require('gulp-log2');

gulp.task('default', function () {
	return gulp.src('foo.js')
		.pipe(log('starting the pipeline!'))
		.pipe(gulp.dest('dist'));
});
```

## API

### log(message, options)

#### message

Type: `string`

Log message to display.

#### options

##### title

Type: `string`  
Default: `'gulp-log'`

Use a custom title to make it easier to distinguish where messages are logged from.

##### level

Type: `string`  
Default: `'info'`

The output level for the log message. Each level has an associated color provided by [Chalk]:

* `'fatal'`: bgRed
* `'error'`: red
* `'warn'`: yellow
* `'info'`: green
* `'debug'`: blue

### log.levels[]

Associative array of output levels and colors. Custom levels can be added here.

Colors are provided by [Chalk], and should be set like so:

```
var chalk = require('chalk');

log.levels['custom'] = chalk.magenta.underline;
```


[Chalk]: https://www.npmjs.com/package/chalk
[npm-url]: https://www.npmjs.com/package/gulp-log2
[npm-image]: https://badge.fury.io/js/gulp-log2.svg
[ci-url]: https://semaphoreci.com/coldacid/gulp-log2
[ci-image]: https://semaphoreci.com/api/v1/coldacid/gulp-log2/branches/master/shields_badge.svg
[depstat-url]: https://david-dm.org/coldacid/gulp-log
[depstat-image]: https://david-dm.org/coldacid/gulp-log.svg
